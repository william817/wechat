<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::any('initToken', 'WeixinController@valid');
//Route::any('get/token', 'WeixinController@aaa');
//Route::get('bbb/{code}', 'WeixinController@bbb');

//获取openid
Route::any('get/code','WeixinController@getCode');
Route::any('get/openid','WeixinController@getOpenid');
Route::any('binding/userno','BindingWechatController@BindingUserno');