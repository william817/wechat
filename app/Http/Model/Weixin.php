<?php

namespace App\Http\Model;

use Illuminate\Database\Eloquent\Model;

class Weixin extends Model
{
    //获取openid
    public static function getOpenid($input){
        $code = $input['code'];
        $weixin =  file_get_contents("https://api.weixin.qq.com/sns/oauth2/access_token?appid=wx4805212c70012f70&secret=3529f4f9ae25c6d1e4d9c46141073f7d&code=".$code."&grant_type=authorization_code");//通过code换取网页授权access_token
        $jsondecode = json_decode($weixin); //对JSON格式的字符串进行编码
        $array = get_object_vars($jsondecode);//转换成数组
        $openId = $array['openid'];
        //$access_token = $array['access_token'];

//        dd($openid);
        return $openId;
    }
}
