<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use EasyWeChat\Factory;
use Illuminate\Support\Facades\Input;
use App\Http\Model\Weixin;
class WeixinController extends Controller
{
    //封装
    // private
    private $token = 'wechat';

    public function valid()
    {
        $echoStr = $_GET["echostr"];
        //valid signature , option
        if ($this->checkSignature()) {
            echo $echoStr;
            exit;
        }
    }

    //检查签名
    public function checkSignature()
    {
        $signature = $_GET["signature"];
        $timestamp = $_GET["timestamp"];
        $nonce = $_GET["nonce"];
        $token = $this->token;
        $tmpArr = array($token, $timestamp, $nonce);
        sort($tmpArr, SORT_STRING);
        $tmpStr = implode($tmpArr);
        $tmpStr = sha1($tmpStr);
        if ($tmpStr == $signature) {
            return true;

        } else {
            return false;
        }
    }


    public function getCode(){
        $REDIRECT_URI='http://www.bainiuhz.com/wechat/public/get/openid';

        $redirect_uri=urlencode($REDIRECT_URI);

        $url='https://open.weixin.qq.com/connect/oauth2/authorize?appid=wx4805212c70012f70&redirect_uri='.$redirect_uri.'&response_type=code&scope=snsapi_base&state=123#wechat_redirect';

        header("Location:".$url);
    }

    //获取openid
    public function getOpenid(){
        Weixin::getOpenid(Input::all());
    }
}
