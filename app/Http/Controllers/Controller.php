<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Input;
use App\Http\Model\Weixin;
class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function __construct() {
        if (Cache::has('openId')) {
            $openId = Cache::get('openId');
            defined("OPENID") OR define("OPENID",$openId);
        } else {
            $openId = $this->getCode();
            Cache::put('openId',$openId,24*3600);
            defined("OPENID") OR define("OPENID",$openId);
        }
    }
    public function getCode(){
        $REDIRECT_URI='http://www.bainiuhz.com/wechat/public/get/openid';

        $redirect_uri=urlencode($REDIRECT_URI);

        $url='https://open.weixin.qq.com/connect/oauth2/authorize?appid=wx4805212c70012f70&redirect_uri='.$redirect_uri.'&response_type=code&scope=snsapi_base&state=123#wechat_redirect';

        header("Location:".$url);
    }

    //获取openid
    public function getOpenid(){
        Weixin::getOpenid(Input::all());
    }
}
